import os, datetime, json
import pika

from common.storage import *
from common.classes import *
from common.utils import *
from common.menu import *


class RabbitServer(Handler):
    def on_request(self, ch, method, props, body):
        def dateconverter(o):
            if isinstance(o, datetime.datetime):
                return o.__str__()

        request = json.loads(body)
        result = self.handle(request.get('command'), request)

        if result != None:
            response = json.dumps(result, default=dateconverter)
        else:
            response = ""

        ch.basic_publish(
            exchange='',
            routing_key=props.reply_to,
            properties=pika.BasicProperties(correlation_id=props.correlation_id),
            body=response
        )

        ch.basic_ack(delivery_tag=method.delivery_tag)

    def run(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=os.getenv('RABBITMQIP', '127.0.0.1')))
        channel = connection.channel()
        channel.queue_declare(queue='rpc_queue')
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue='rpc_queue', on_message_callback=self.on_request)
        channel.start_consuming()


def main():
    airline = Airline(DB("rpc"), get_dict_io())

    server = RabbitServer()

    @server.set_handler("get_item")
    def get_item(data):
        return airline.get_item(data.get('id'))

    @server.set_handler("get_items")
    def get_items(data):
        items = []
        for item in airline.storage.get_items():
            items.append(item.get_data())
        return items

    @server.set_handler("add_item")
    def add_item(data):
        airline.io.set_source(data.get('data'))
        airline.process_item()

    @server.set_handler("delete_item")
    def delete_item(data):
        airline.delete_item(data.get('id'))

    @server.set_handler("clean")
    def clean(data):
        airline.clean()

    server.run()


main()
