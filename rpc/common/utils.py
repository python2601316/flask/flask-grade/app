from .io import *
from flask import g
from flask import request


def get_rest_io():
    if 'rest' not in g:
        g.rest = Rest()
    return g.rest


def get_flask_io():
    if 'flask' not in g:
        g.flask = Flask()
    return g.flask


def get_web_io():
    if '/api/' in request.path:
        return get_rest_io()
    return get_flask_io()


def get_console_io():
    if not get_console_io.console:
        get_console_io.console = Console()
    return get_console_io.console


get_console_io.console = None


def get_dict_io():
    if not get_dict_io.dict:
        get_dict_io.dict = Dict()
    return get_dict_io.dict


get_dict_io.dict = None
