from dataclasses import dataclass
from .worker import Worker


@dataclass
class Pilot(Worker):
    subject: str = ''
    class_type: int = 1

    def input(self, io):
        Worker.input(self, io)
        self.subject = io.input_field('subject', 'Subject', self.subject)

    def __str__(self):
        return f"""
            ID: {self.id}	
            Name: {self.name}	
            Age: {self.age}	
            Salary: {self.salary}
            Subject: {self.subject}
        --	
        """
