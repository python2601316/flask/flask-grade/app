from dataclasses import dataclass


@dataclass
class Worker:
    id: int = 0
    name: str = ''
    age: int = 0
    salary: int = 0
    class_type: int = 0

    def get_data(self):
        return self.__dict__

    def set_data(self, d):
        if d:
            self.__dict__.update(d)

    def get_info(self):
        return [self.id, self.name]

    def input(self, io):
        self.name = io.input_field('name', 'Name', self.name)
        self.age = io.input_field('age', 'Age', self.age)
        self.salary = io.input_field('salary', 'Salary', self.salary)

    def output(self, io):
        return io.output_item(self)

    def edit(self, io):
        return io.edit_item(self)

    def __str__(self):
        return f"""
            ID: {self.id}	
            Name: {self.name}	
            Age: {self.age}	
            Salary: {self.salary}	
        --	
        """
