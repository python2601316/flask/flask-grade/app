class Menu:
    def __init__(self):
        self.handlers = list()

    def add_handler(self, title):
        def decorator_set_handler(handler):
            self.handlers.append([title, handler])
            return handler

        return decorator_set_handler

    def run(self):
        while True:
            try:
                print()
                for i, item in enumerate(self.handlers, 1):
                    print(f"{i}. {item[0]}")
                command = int(input("Input command: ")) - 1
                self.handlers[command][1]()
                if self.handlers[command][0] == "Exit":
                    break
            except Exception as e:
                print(e)
#				raise(e)


class Handler:
    def __init__(self):
        self.handlers = dict()

    def set_handler(self, command):
        def decorator_set_handler(handler):
            self.handlers[command] = handler
            return handler

        return decorator_set_handler

    def handle(self, command, data):
        if command in self.handlers:
            return self.handlers[command](data)
