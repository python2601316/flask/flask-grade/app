from flask import Blueprint, g
from common.classes import *
from common.storage import *
from common.utils import *

bp = Blueprint('mongoAirline', __name__)


def get_airline():
    if 'airline' not in g:
        g.airline = Airline(Mongo(), get_web_io())
    return g.airline


@bp.route("/", methods=['GET'])
def index():
    return get_airline().output()


@bp.route("/edit/<id>", methods=['GET'])
def edit_item(id):
    return get_airline().edit_item(id)


@bp.route("/delete/<id>", methods=['GET'])
def delete_item(id):
    return get_airline().delete_item(id)


@bp.route("/process/<id>", methods=['POST'])
def process_item(id):
    return get_airline().process_item(id)


@bp.route("/api/", methods=['GET'])
def api_index():
    return get_airline().output()


@bp.route("/api/<id>", methods=['GET'])
def api_get_item(id):
    return get_airline().get_item(id)


@bp.route("/api/", methods=['POST'])
def api_add_item():
    return get_airline().process_item(0)


@bp.route("/api/<id>", methods=['PUT'])
def api_set_item(id):
    return get_airline().process_item(id)


@bp.route("/api/<id>", methods=['DELETE'])
def api_delete_item(id):
    return get_airline().delete_item(id)


@bp.route("/api/", methods=['DELETE'])
def api_clean():
    return get_airline().clean()


@bp.teardown_request
def teardown_book(ctx):
    get_airline().store()
