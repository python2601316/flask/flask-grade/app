from flask import Flask
from flask import render_template

app = Flask(__name__)

from app.sqliteAirline import bp as bp1
from app.pickleAirline import bp as bp2
from app.postgresqlAirline import bp as bp3
from app.mongoAirline import bp as bp4
from app.rabbitmqAirline import bp as bp5

airline_bps = [
    ["SQLite Airline", bp1, "/sqliteAirline"],
    ["Pickle Airline", bp2, "/pickleAirline"],
    ["PostgreSQL Airline", bp3, "/postgresqlAirline"],
    ["Mongo Airline", bp4, "/mongoAirline"],
    ["RabbitMQ Airline", bp5, "/rabbitmqAirline"],
]

for title, bp, url in airline_bps:
    app.register_blueprint(bp, url_prefix=url)


@app.route("/")
def index():
    r = ""
    for title, bp, url in airline_bps:
        r += f'<a href="{url}">{title}</a><br>'
    return render_template("index.tpl", airlines=r)
