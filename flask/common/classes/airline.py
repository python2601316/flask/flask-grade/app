from .worker import Worker
from .pilot import Pilot


class Airline:
    def __init__(self, storage, io):
        self.storage = storage
        self.io = io

    def load(self):
        self.storage.load()

    def store(self):
        self.storage.store()

    def clean(self):
        self.storage.clean()

    def output(self):
        return self.io.output(self.storage.get_items())

    def get_item(self, id):
        return self.io.output_item(self.storage.get_item(id) or Worker())

    def edit_item(self, id):
        return self.io.edit_item(self.storage.get_item(id) or Worker())

    def process_item(self, id=None):
        if id is None:
            id = self.io.input_field("id", title="ID")
        type = self.io.input_field("subject")
        item = self.storage.get_item(id) or (Pilot() if type else Worker())
        item.input(self.io)
        self.storage.add_item(item)
        return self.io.default_output(self)

    def delete_item(self, id=None):
        if id == None:
            id = self.io.input_field("id", title="ID")
        self.storage.delete_item(id)
        return self.io.default_output(self)
