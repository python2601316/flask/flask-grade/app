from flask import request
from flask import jsonify


class Rest:
	def default_output(self, book):
		return ""

	def output(self, getItems):
		ids = []
		for item in getItems:
			ids.append(item.get_info())
		return jsonify({'ids': ids})

	def input_field(self, field, title=None, default=None):
		return request.json.get(field, default)

	def output_item(self, item):
		if item:
			return jsonify(item.getData())
		return ""

	def edit_item(self, item):
		pass

