from flask import render_template
from flask import request


class Flask:
    def default_output(self, item):
        return item.output()

    def output(self, items):
        return render_template('airlines.tpl', items=items)

    def input_field(self, field, title=None, default=None):
        return request.form.get(field, default)

    def output_item(self, item):
        return render_template('item.tpl', item=item)

    def edit_item(self, item):
        return render_template('form.tpl', item=item)
