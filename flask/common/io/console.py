class Console:
    def default_output(self, book):
        return ""

    def output(self, items):
        for item in items:
            print(item)

    def input_field(self, field, title=None, default=None):
        if field == "id" and default != None:
            return default
        return input(f"{title}: ")

    def output_item(self, item):
        return str(item)

    def edit_item(self, item):
        pass
