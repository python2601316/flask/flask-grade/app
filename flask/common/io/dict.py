class Dict:
	def default_output(self, book):
		return ""

	def set_source(self, source):
		self.source = source

	def input_field(self, field, title=None, default=None):
		return self.source.get(field, default)

	def output_item(self, item):
		return item.get_data()
