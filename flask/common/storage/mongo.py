import os
import pymongo
from bson.objectid import ObjectId

from ..classes import *


class Mongo:
    def __init__(self):
        self.load()

    def load(self):
        self.client = pymongo.MongoClient(os.getenv('DBMONGOCONNECT', 'mongodb://olga:pass@127.0.0.1:27017/'))
        self.db = self.client["stud_flask_db"]
        self.dbc = self.db["stud_flask_db"]

    def store(self):
        self.client.close()

    def clean(self):
        self.dbc.delete_many({})

    def get_item(self, id):
        obj = self.__pre_process(self.dbc.find_one({"_id": ObjectId(id)})) if id != '0' else None
        if obj:
            item = self.__correct_dataclass(obj)
            item.set_data(obj)
        else:
            item = None
        return item

    def add_item(self, item):
        if not item.id or item.id == '0':
            self.dbc.insert_one(item.get_data())
        else:
            self.dbc.update_one({"_id": ObjectId(item.id)}, {"$set": item.get_data()})

    def delete_item(self, id):
        self.dbc.delete_one({"_id": ObjectId(id)})

    def get_items(self):
        for obj in self.dbc.find():
            item = self.__correct_dataclass(obj)
            item.set_data(self.__pre_process(obj))
            yield item

    def __correct_dataclass(self, r):
        return Pilot() if r['class_type'] else Worker()

    def __pre_process(self, r):
        r['id'] = r['_id']
        del r['_id']
        return r

