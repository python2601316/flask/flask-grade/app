import datetime
import os
import sqlite3

from ..classes import *


class DB:
	def __init__(self, name="", placeholder='?'):
		self.directory = "data/" + name
		self.placeholder = placeholder
		self.load()

	def get_init_fields(self):
		return "id integer primary key autoincrement"

	def init_table(self):
		item = Pilot()
		fields = self.get_init_fields()
		for field, value in item.get_data().items():
			if field == "id":
				continue
			fields += f", {field} "

			if isinstance(value, int):
				fields += "integer"
			elif isinstance(value, datetime.datetime):
				fields += "timestamp"
			else:
				fields += "text"

		self.dbc.execute(f"create table if not exists airline_employees({fields})")

	def load(self):
		if not os.path.exists(self.directory):
			os.makedirs(self.directory)
		self.db = sqlite3.connect(self.directory + '/airline.sqlite', detect_types=sqlite3.PARSE_DECLTYPES)
		self.db.row_factory = sqlite3.Row
		self.dbc = self.db.cursor()
		self.init_table()

	def store(self):
		self.db.close()

	def clean(self):
		self.dbc.execute(f"delete from airline_employees")
		self.db.commit()

	def get_item(self, id):
		self.dbc.execute(f"select * from airline_employees where id={self.placeholder}", (int(id),))
		row = self.dbc.fetchone()
		if row:
			item = Pilot() if row['class_type'] else Worker()
			item.set_data(row)
		else:
			item = None
		return item

	def add_item(self, item):
		names = ""
		values = ""
		params = []
		update = ""
		for field, value in item.get_data().items():
			if field == "id":
				continue
			if names:
				names += f", {field}"
				values += f", {self.placeholder}"
				update += f", {field} = {self.placeholder}"
			else:
				names = field
				values = self.placeholder
				update += f"{field} = {self.placeholder}"
			if isinstance(value, datetime.datetime):
				params.append(self.make_time(value))
			else:
				params.append(value)

		if not item.id or int(item.id) == 0:
			self.dbc.execute(f"insert into airline_employees({names}) values({values})", params)
		else:
			params.append(int(item.id))
			self.dbc.execute(f"update airline_employees set {update} where id={self.placeholder}", params)
		self.db.commit()

	def delete_item(self, id):
		self.dbc.execute(f"delete from airline_employees where id={self.placeholder}", (int(id),))
		self.db.commit()

	def get_items(self):
		self.dbc.execute("select * from airline_employees order by id desc")
		for r in self.dbc:
			item = Pilot() if r['class_type'] else Worker()
			item.set_data(r)
			yield item

	def make_time(self, time):
		return time
