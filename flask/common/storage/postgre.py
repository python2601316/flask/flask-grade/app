import os
import psycopg2
import psycopg2.extras
from .db import *


class Postgre(DB):
    def __init__(self):
        super().__init__(placeholder='%s')

    def get_init_fields(self):
        return "id serial primary key"

    def load(self):
        self.db = psycopg2.connect(user=os.getenv('DBGPUSER', 'olga'), password=os.getenv('DBPGPASS', 'pass'),
                               host=os.getenv('DBPGIP', '127.0.0.1'), database=os.getenv('DBPG', 'stud_flask_db'))
        self.dbc = self.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.init_table()

    def store(self):
        self.dbc.close()
        self.db.close()
