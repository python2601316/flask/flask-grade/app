import os, datetime
import pika, uuid, json
from ..classes import *


class Rabbit:
    class RpcClient(object):
        def __init__(self):
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=os.getenv('RABBITMQIP', '127.0.0.1')))

            self.channel = self.connection.channel()

            result = self.channel.queue_declare(queue='', exclusive=True)
            self.callback_queue = result.method.queue

            self.channel.basic_consume(
                queue=self.callback_queue,
                on_message_callback=self.on_response,
                auto_ack=True,
            )

            self.response = None
            self.corr_id = None

        def disconnect(self):
            self.connection.close()

        def on_response(self, ch, method, props, body):
            if self.corr_id == props.correlation_id:
                self.response = body

        def call(self, data):
            def dateconverter(o):
                if isinstance(o, datetime.datetime):
                    return o.__str__()

            self.response = None

            self.corr_id = str(uuid.uuid4())

            self.channel.basic_publish(
                exchange='',
                routing_key='rpc_queue',
                properties=pika.BasicProperties(
                    reply_to=self.callback_queue,
                    correlation_id=self.corr_id,
                ),
                body=json.dumps(data, default=dateconverter),
            )

            self.connection.process_data_events(time_limit=None)

            try:
                return json.loads(self.response)
            except:
                return None

    def __init__(self):
        self.rpc = self.RpcClient()

    def load(self):
        pass

    def store(self):
        self.rpc.disconnect()

    def call(self, command, id=0, data=''):
        return self.rpc.call({'command': command, 'id': id, 'data': data})

    def get_item(self, id):
        item = Pilot()
        if int(id) > 0:
            res = self.call('get_item', id)
            item.set_data(res)
        return item

    def add_item(self, item):
        self.call('add_item', item.id, item.get_data())

    def delete_item(self, id):
        self.call('delete_item', id)

    def clean(self):
        self.call('clean')

    def get_items(self):
        res = self.call('get_items')
        for r in res:
            item = Pilot()
            item.set_data(r)
            yield item
