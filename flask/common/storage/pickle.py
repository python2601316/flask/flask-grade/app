import os
import pickle
from ..classes import *


class Pickle:
    def __init__(self, name):
        self.directory = "data/" + name
        try:
            self.load()
        except:
            self.clean()

    def load(self):
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        with open(self.directory + '/airline.pickle', 'rb') as f:
            (self.maxID, self.items) = pickle.load(f)

    def store(self):
        with open(self.directory + '/airline.pickle', 'wb') as f:
            pickle.dump((self.maxID, self.items), f)

    def clean(self):
        self.items = {}
        self.maxID = 0

    def get_item(self, id):
        id = int(id)
        if id in self.items:
            return self.items[id]
        return None

    def add_item(self, item):
        item.id = int(item.id)
        if item.id <= 0:
            self.maxID += 1
            item.id = self.maxID
            self.items[item.id] = item

    def delete_item(self, id):
        id = int(id)
        if id in self.items:
            del self.items[id]

    def get_items(self):
        for id, item in self.items.items():
            yield item
