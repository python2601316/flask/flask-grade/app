DOCKER_COMPOSE_PATH = ./docker-compose.yml
PROJECT_NAME = stud-flask-app

DOCKER_COMPOSE_BASE_COMMAND = docker compose -p ${PROJECT_NAME} -f ${DOCKER_COMPOSE_PATH}

run: ## run project
	@${DOCKER_COMPOSE_BASE_COMMAND} up -d

stop: ## stop project
	@${DOCKER_COMPOSE_BASE_COMMAND} stop

restart: ## restart project
	make stop; make run

rebuild-services: ## rebuild services containers defined in command arg
	@for s in $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)); do \
		${DOCKER_COMPOSE_BASE_COMMAND} stop $$s; \
		${DOCKER_COMPOSE_BASE_COMMAND} rm -f $$s; \
		${DOCKER_COMPOSE_BASE_COMMAND} up -d --no-deps --build $$s; \
	done


